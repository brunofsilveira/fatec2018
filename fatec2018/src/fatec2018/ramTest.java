/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fatec2018;

/**
 * Copyright (c) 2001-2018 Mathew A. Nelson and Robocode contributors All rights
 * reserved. This program and the accompanying materials are made available
 * under the terms of the Eclipse Public License v1.0 which accompanies this
 * distribution, and is available at
 * http://robocode.sourceforge.net/license/epl-v10.html
 */
import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

import java.awt.*;
import robocode.AdvancedRobot;

public class ramTest extends AdvancedRobot {

    int sentidoRotacao = 1; // Define se o robo vai girar no sentido Horario oU Anti Horario

    public void run() { // tudo o que estiver dentro deste parametro vai rodar a partir do start
        // Definicoes de Cores
        setBodyColor(Color.lightGray);
        setGunColor(Color.gray);
        setRadarColor(Color.darkGray);

        while (true) {
            turnRight(5 * sentidoRotacao);
            //turnRadarRight(5 * sentidoRotacao);
        }
    }

    
    public void onScannedRobot(ScannedRobotEvent e) {

        String aliado1 = "samplesentry.BorderGuard"; //informar o nome do aliado 1
        String aliado2 = "fatec2018.ramTest* (1)"; //informar o nome do aliado 2
        String aliado3 = "fatec2018.ramTest* (2)"; //informar o nome do aliado 2
        String aliado4 = "fatec2018.ramTest* (3)"; //informar o nome do aliado 2

        String nomeRobo = getName();  //captura o nome do robo detectado

        if (nomeRobo.equals(aliado1) || nomeRobo.equals(aliado2) || nomeRobo.equals(aliado3) || nomeRobo.equals(aliado4)) { //caso o nome do robo detectado seja igual ao dos robôs aliados ele não vai disparar
            setTurnRight(10);
        }

        
        if (e.getBearing() >= 0) { 
            sentidoRotacao = 1;
        } else {
            sentidoRotacao = -1;
        }
        
        setTurnRight(e.getBearing());
        setAhead(e.getDistance() - 100);
        
        
        if (e.getDistance() <= 200) {  // Se o tank estiver a 200 ou menos do inimigo ira rodar...
            if (e.getEnergy() > 16) {  
                fire(3); //se a energia for maior que 16, tiro 3
            } else if (e.getEnergy() > 10) {
                fire(2); //se a energia for maior que 16, tiro 2
            } else if (e.getEnergy() > 4) {
                fire(1); //se a energia for maior que 16, tiro 1
            } else if (e.getEnergy() > 2) {
                fire(.5); //se a energia for maior que 16, tiro 0.5
            } else if (e.getEnergy() > .4) {
                fire(.1); //se a energia for maior que 16, tiro 0.1
            }            
        }
        scan(); //torna a escanear novamente
    }

    /**
     * onHitRobot: Turn to face robot, fire hard, and ram him again!
     */
    
    public void onHitRobot(HitRobotEvent e) {
        if (e.getBearing() >= 0) {
            sentidoRotacao = 1;
        } else {
            sentidoRotacao = -1;
        }
        turnRight(e.getBearing());

    }
}
